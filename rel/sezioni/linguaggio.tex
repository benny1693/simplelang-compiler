Il linguaggio che bisognava definire doveva avere le caratteristiche basilari presenti nei linguaggi imperativi con le seguenti semplificazioni:
\begin{itemize}[nosep]
	\item non esistono chiamate di procedura: il programma è costituito esclusivamente dal \textit{main};
	\item l'unico tipo richiesto per le variabili è quello degli interi.
\end{itemize}
\vspace*{0.5em}
I linguaggio richiesto deve avere almeno le seguenti caratteristiche:
\begin{itemize}[nosep]
	\item le variabili devono essere dichiarate prima del loro utilizzo e non possono essere dichiarate più volte nello stesso blocco;
	\item avere litterali interi, possibilmente preceduti da un simbolo di segno;
	\item avere le seguenti istruzioni di base:
	\begin{itemize}[nosep]
		\item dichiarazione;
		\item assegnazione;
		\item espressioni aritmetiche con parentesi tonde;
		\item espressioni booleane con parentesi tonde;
		\item operazioni di confronto;
		\item istruzione condizionale;
		\item istruzione di iterazione;
		\item un'istruzione di stampa per mostrare il valore delle variabili.
	\end{itemize}
\end{itemize}

\subsection{Definizione di \textit{SimpleLang}}
\textit{SimpleLang} cerca di unire le caratteristiche che più mi sono piaciute nel corso della mia (breve) esperienza da programmatore: in particolare, mi piace molto l'idea di avvicinare la sintassi del codice a quella naturale.\\ 
Nonostante questo, non ho voluto comunque appesantire il linguaggio rispetto a quella che è la prassi: ho evitato di usare \textit{keywords} non necessarie (ad esempio per la dichiarazione e l'assegnazione) e ho cercato di mantenere la sintassi dei costrutti di \textit{control flow} il più simile possibile a quella definita nei linguaggi di programmazione più conosciuti.\\
Un elemento in più che ho aggiunto al mio progetto è la possibilità di definire variabili booleane. Ciò ha implicato la necessità di definire delle regole per gli operatori in base ai tipi delle variabili coinvolte: l'approccio che ho adottato è rigido e impedisce il \textit{cast} da interi a booleani o viceversa.
	
	\subsubsection{Parole chiave}
		In questa sezione presento la lista delle \textit{keywords} del linguaggio:\\
		\begin{minipage}{\linewidth}
			\hspace*{10em}
			\begin{tabular}{r | l}
				tipi		& \texttt{bool}\\
							&	\texttt{int}\\
				costanti booleane	& \texttt{True}\\
								          	& \texttt{False}\\
				operatori logici 	&	\texttt{and}\\
													&	\texttt{or}\\
													&	\texttt{not}\\
				costrutto condizionale 	&	\texttt{if}\\
																&	\texttt{then}\\
																&	\texttt{else}\\
				costrutti iterativi	&	\texttt{while}\\
														&	\texttt{do}\\
													 	&	\texttt{for}\\
														&	\texttt{to}\\
				stampa		& \texttt{print}\\
			\end{tabular}
		\end{minipage}
		
	\subsubsection{Identificatori}
		Gli identificatori hanno una lunghezza massima pari a 31 caratteri: questa scelta è giustificata dal fatto che secondo diversi studi l'utilizzo di nomi troppo lunghi per gli identificatori ha un impatto negativo sia sulla mantenibilità del codice, che sulla sua correttezza \cite{BINKLEY2009,SCALABRINO2018}.\\
		Inoltre, conoscere un limite prefissato per il nome della variabile permette una gestione più efficiente delle stringhe durante la traduzione. Questa soluzione è adottata anche in altri linguaggi come MATLAB e SQL.\\
		Gli identificatori sono delle stringhe composte da caratteri alfanumerici e dal carattere \textit{underscore} e non possono iniziare con un numero.
		
	\subsubsection{Litterali}
		Dal momento che \textit{SimpleLang} dispone sia di booleani che di interi, esistono i litterali per entrambi i tipi.
		
		\paragraph{Booleani} Per i booleani ho definito due litterali che indicano i valori ``vero'' (\texttt{True}) e ``falso'' (\texttt{False}).
		
		\paragraph{Interi} Per gli interi è possibile fornire una qualsiasi sequenza di cifre, in cui quella più significativa deve essere diversa da 0. Inoltre, deve essere possibile scrivere un simbolo di segno prima del numero. Ecco alcuni esempi: \texttt{2043}, \texttt{-221}, \texttt{+987}.
		
	\subsubsection{Operazioni disponibili}
		Le operazioni disponibili sono quelle aritmentiche, quelle logiche e quelle di confronto. Come già detto, non è possibile eseguire operazioni aritmetiche o di confronto tra booleani e, viceversa, non è possibile effettuare operazioni logiche tra interi.\\
		Come solitamente avviene per la maggior parte dei linguaggi di programmazione, le operazioni binarie sono scritte in notazione infissa, mentre quelle unarie (come la negazione logica) in notazione prefissa:
		\begin{center}
			numero \texttt{binop} numero\\
			\texttt{op} numero
		\end{center}
		dove \texttt{binop} è un'operazione binaria e \texttt{op} è un'operazione unaria.	
	
		\paragraph{Operazioni aritmetiche}
			Le operazioni aritmetiche disponibili sono
			\begin{itemize}[nosep]
				\item addizione: \texttt{+}
				\item sottrazione: \texttt{-}
				\item moltiplicazione: \texttt{*}
				\item divisione: \texttt{/} 
			\end{itemize}
			
		\paragraph{Operazioni logiche}	
			Le operazioni logiche disponibili sono
			\begin{itemize}[nosep]
				\item congiunzione: \texttt{and} 
				\item disgiunzione: \texttt{or}
				\item negazione: \texttt{not}
			\end{itemize}
			Ho scelto di utilizzare le parole in lingua inglese per gli operatori logici per rendere le condizioni booleane più espressive e più simili al linguaggio naturale, come avviene ad esempio in \textit{Python}.
					 
		\paragraph{Operazioni di confronto}
			Le operazioni di confronto disponibili sono
			\begin{itemize}[nosep]
				\item disuguaglianze: \texttt{<=}, \texttt{>=}
				\item disuguaglianze strette: \texttt{<}, \texttt{>}
				\item uguaglianza: \texttt{==}
				\item diversità: \texttt{!=} 
			\end{itemize}
			
		
		\paragraph{Precedenze}
			Gli operatori aritmetici hanno precedenza rispetto agli altri tipi di operatori. Seguono quelli di confronto e, infine, quelli logici. In questo modo, l'espressione $1 + 2 < 2 + 3 \texttt{ and } 1 < 2$ viene valutata come:
			\begin{equation*}
			((1 + 2) < (2 + 3)) \texttt{ and } (1 < 2)
			\end{equation*}
			In tabella 1, è presente la lista degli operatori con i relativi gradi di precedenza (6 è la priorità massima e 1 è la priorità minima).
			Gli operatori di confronto non sono associativi, contrariamente a tutti gli altri.			
			\begin{table}
				\centering
				\caption{Operatori e precedenza}
				\begin{tabular}{| c | c |}
					\hline
					\textbf{Operatore} & \textbf{Precedenza} \\\hline
					\multicolumn{2}{| c |}{Aritmetici}\\\hline
						\texttt{+} & 5\\
						\texttt{-} & 5\\
						\texttt{*} & 6\\
						\texttt{/} & 6\\\hline
					\multicolumn{2}{| c |}{Logici}\\\hline
						\texttt{and} & 2\\
						\texttt{or}  & 1\\
						\texttt{not} & 3\\\hline
					\multicolumn{2}{| c |}{Confronto}\\\hline
						\texttt{<}  & 4\\
						\texttt{<=} & 4\\
						\texttt{>}  & 4\\
						\texttt{>=} & 4\\
						\texttt{==} & 4\\
						\texttt{!=} & 4\\\hline
				\end{tabular}
			\end{table}			 
		
	\subsubsection{\textit{Statements}}	
		I singoli \textit{statements} di \textit{SimpleLang} finiscono con il punto e virgola (\texttt{;}) e possono essere contenuti in blocchi delimitati da parentesi graffe (\texttt{\{} , \texttt{\}}). L'esempio qui riportato mostra una sequenza di istruzioni valida:
		\begin{lstlisting}[escapeinside={@}{@}]
x : int = 1;
b : bool;
{
    x : int = 3;
    b = True;
}
{
    b : int = 0;
}
		\end{lstlisting}\vspace*{0.5em}
		Una variabile definita all'interno di un blocco è locale a quel blocco. Se una variabile già esistente viene definita nuovamente in un blocco più interno, viene rispettata la regola dello \textit{shadowing}.\\
		La scelta di adottare un \textit{token} di fine istruzione e di usare le parentesi graffe come delimitatori di blocco allontana il linguaggio da quello naturale, ma lo rende più pratico da usare. Indicando il termine di un'istruzione, è possibile scrivere più \textit{statements} in una stessa riga (utile se sono brevi). Invece, il dover esplicitare l'inizio e la fine di un blocco migliora la mantenibilità del codice, contrariamente a quanto accade sfruttando l'indentazione: gli errori causati da quest'ultima sono molto difficili da identificare (soprattutto per i programmatori meno esperti). Ho, inoltre, scartato la possibilità di usare \texttt{begin} ed \texttt{end} come delimitatori perché li ritengo troppo verbosi.\\
			
		\paragraph{Dichiarazione}
			\`E possibile dichiarare una variabile inizializzandola esplicitamente o meno. Nel secondo caso, le variabili intere vengono inizializzate a 0, mentre quelle booleane a \texttt{False}.\\ 
			La dichiarazione con inizializzazione può essere scritta nei modi seguenti:
			\begin{lstlisting}[escapeinside={@}{@}]
x : int = @\textit{espressione aritmetica}@;	
b : bool = @\textit{espressione logica}@;
			\end{lstlisting}\vspace*{0.5em}
			La dichiarazione senza inizializzazione, invece, può essere scritta nei modi seguenti:
			\begin{lstlisting}[escapeinside={@}{@}]
x : int;	
b : bool;
			\end{lstlisting}
		
		\paragraph{Assegnazione}
			L'istruzione di assegnazione è definita come in molti linguaggi di programmazione tramite l'operatore \texttt{=}.
			\begin{lstlisting}[escapeinside={@}{@}]
v = @\textit{espressione}@;	
			\end{lstlisting}
			
		\paragraph{Stampa}
			Esistono due tipi di istruzioni di stampa: una con un'espressione all'interno e una vuota. La prima tipologia stampa semplicemente il valore dell'espressione fornita, mentre la seconda serve ad andare a capo.
			\begin{lstlisting}[escapeinside={@}{@}]
print(@\textit{espressione}@);

print();	
			\end{lstlisting}\vspace*{0.5em}
			Per chiarezza, il seguente esempio mostra l'output generato dalle istruzioni di stampa in esso contenute.
			\begin{lstlisting}[escapeinside={@}{@}]
print(1);
print(0);
print();
print(0);
print(1);
			\end{lstlisting}
			
			\begin{lstlisting}[escapeinside={@}{@}]
10
01			
			\end{lstlisting}
			
	\subsubsection{Costrutti disponibili}
		I costrutti di \textit{control flow} disponibili possono contenere una serie di istruzioni delimitate obbligatoriamente da un blocco oppure una singola istruzione senza indicare il blocco. Questo secondo caso corrisponde a un blocco contenente una singola istruzione.
		
		\paragraph{Istruzione condizionale}
			Il costrutto condizionale è nella forma più comunemente presente nei linguaggi di programmazione sia nella forma \textit{if-then} che nella forma \textit{if-then-else}:
			\begin{lstlisting}[escapeinside={@}{@}]
if @\textit{condition}@ then {
  @\textit{codice}@
}

if @\textit{condition}@ then 
  @\textit{statement}@;
  
if @\textit{condition}@ then {
  @\textit{codice}@
} else {
	@\textit{codice}@
}

if @\textit{condition}@ then 
  @\textit{statement}@;
else
	@\textit{statement}@;
			\end{lstlisting}\vspace*{0.5em}
			Nel caso \textit{if-then-else}, è possibile anche che uno dei due rami abbia un blocco e l'altro un singolo \textit{statement}. 
			
		\paragraph{Istruzioni iterative}
			Il linguaggio mette a disposizione due tipi di costrutti iterativi molto comuni	nei linguaggi di programmazione moderni: l'istruzione \textit{while-do} e l'istruzione \textit{for}. Anche in questo caso, è possibile usare questi costrutti senza indicare il blocco.
			\begin{lstlisting}[escapeinside={@}{@}]
while @\textit{condition}@ do {
  @\textit{codice}@
}

while @\textit{condition}@ do 
  @\textit{statement}@;
  
for @\textit{dichiarazione/assegnazione}@ to @{numero}@ {
  @\textit{codice}@
}

for @\textit{dichiarazione/assegnazione}@ to @{numero}@
  @\textit{statement}@;
			\end{lstlisting}\vspace*{0.5em}
			Il ciclo \textit{for} parte dal valore iniziale della variabile dichiarata o assegnata fino al numero indicato incluso. 
			In sostanza, un ciclo \textit{for} in \textit{SimpleLang} equivale semanticamente a
			\begin{lstlisting}[escapeinside={@}{@}]			
{
  @\textit{dichiarazione/assegnazione}@
  while @\textit{variabile dichiarata/assegnata}@ <= @\textit{numero}@ do {
    @\textit{corpo del ciclo for}@
    @\textit{incremento della variabile}@
	}
}
			\end{lstlisting}\vspace*{0.5em}			
			I seguenti cicli \textit{for} producono tutti lo stesso output.
			\begin{lstlisting}[escapeinside={@}{@}]			
for i : int to 9
  print(i);
  
for i : int = 0 to 9
	print(i);
	
i : int;
for i = 0 to 9
	print(i);
			\end{lstlisting}\vspace*{0.5em}
			La differenza sostanziale tra questi tre tipi di ciclo è che alla fine dei primi due non sarà possibile più accedere alla variabile \texttt{i}, mentre nell'ultimo caso sarà possibile.			
