# SimpleLang Compiler

Progetto didattico del corso di Compilatori del CdL Magistrale in Ingegneria Informatica (UniPD - A.A. 2019-2020).

La consegna prevede la realizzazione di un compilatore che traduce da un linguaggio di programmazione definito dallo studente in Three-Address-Code (3AC).

Il linguaggio usato deve avere le seguenti feature di base:
- variabili di tipo int
- litterali interi, possibilmente preceduti da un simbolo di segno
- le istruzioni di base:
  - dichiarazione
  - assegnazione
  - espressioni aritmetiche con parentesi tonde
  - espressioni booleane con parentesi tonde
  - operazioni di confronto
  - istruzione condizionale
  - istruzione di iterazione
  - un'istruzione di stampa per mostrare il valore delle variabili

