#include "variable.h"

int addVar(char *id, int type, variable** symTable){
    variable *tmp;
    HASH_FIND_STR(*symTable, id, tmp);

    if (tmp == NULL){
        tmp = (variable*) malloc(sizeof(variable));
        tmp->id = id;
        tmp->value = 0;
        tmp->type = type;
        HASH_ADD_KEYPTR(hh, *symTable, tmp->id,strlen(tmp->id), tmp);
        return 1;
    } 

    fprintf(stderr,"ERROR (line %d): multiple definitions for variable %s\n",lineno,id);
    return 0;
}

int setVar(char *id, int type ,int value, variable** symTable){
    variable *tmp;
    HASH_FIND_STR(*symTable, id, tmp);
    if(tmp == NULL){
        fprintf(stderr,"ERROR (line %d): variable %s is not defined\n",lineno,id);
        return 0;
    }
    if (tmp->type != type){
        fprintf(stderr,"ERROR (line %d): the type of variable %s is not %d\n",lineno,id,type);
        return 0;
    }
    tmp->value = value;
    return 1;
}

int getVar(char *id, variable** symTable){
    variable *tmp;
    HASH_FIND_STR(*symTable, id, tmp);
    if(tmp == NULL){
        fprintf(stderr,"ERROR (line %d): variable %s is not defined\n",lineno,id);
        return 0;
    }

    return tmp->value;
}

int getVarType(char *id, variable** symTable){
    variable *tmp;
    HASH_FIND_STR(*symTable, id, tmp);
    if(tmp == NULL){
        fprintf(stderr,"ERROR (line %d): variable %s is not defined\n",lineno,id);
        return -1;
    }

    return tmp->type;
}

void delete_all(variable** symTable) {
  variable *current, *tmp;

  HASH_ITER(hh, *symTable, current, tmp) {
    HASH_DEL(*symTable,current);  
    free(current);            
  }
}