#include "stack.h"

void init(stack* s) {
    s->top = -1;
    s->capacity = 32;
    s->contents = (element*)malloc(sizeof(element) * s->capacity);
    push(s,NULL);
}

void destroy(stack *s) {
    for (int i = 0; i < s->capacity; ++i){
        delete_all(&(s->contents[i]));
        free(s->contents[i]);
    }
    
    free(s->contents);

    s->contents = NULL;
    s->capacity = 0;
    s->top = -1;
}

void push(stack* s, element data) {
    if (isfull(s)) {
        element* tmp = s->contents;
        s->contents = (element*)malloc(sizeof(element) * s->capacity * 2);
        for (int i = 0; i < s->capacity; ++i)
            s->contents[i] = tmp[i];
        s->capacity *= 2;
    }
    (s->top)++;
    s->contents[s->top] = data;
}

void pop(stack* s) {
    if (isempty(s)) {
        fprintf(stderr, "Stack is empty.\n");
        exit(-1);
    }
    delete_all(peek(s,s->top)); // elimina i valori nella symbol table nel top dello stack
    s->top--;
}

element* peek(stack* s,int n) {
    if (isempty(s)) {
        fprintf(stderr, "Stack is empty.\n");
        exit(-1);
    }

    if ((n > s->top) || (n < 0)){
        fprintf(stderr, "Index out of range.\n");
        exit(-1);
    }

    return &(s->contents[n]);
}

element find(stack* s, char* id) {
    element tmp = NULL;
    for (int i = s->top; i >= 0; --i) {
        HASH_FIND_STR(*peek(s,i), id, tmp);
        if (tmp != NULL){
            return tmp;
        }
    }
    return NULL;
}

int isempty(stack* s) {
    return s->top < 0;
}

int isfull(stack* s) {
    return s->top >= (s->capacity - 1);
}

int addVariable(stack* s, char* id, int type) {
    return addVar(id,type,peek(s,s->top));
}

int setVariable(stack* s, char* id, int type, int value) {
    if (type < 0)
        return 0;
    
    element tmp = find(s,id);
    if (tmp == NULL) {
        fprintf(
            stderr,
            "ERROR (line %d): variable %s is not defined\n",
            lineno,id
        );
        return 0;
    } 

    if (tmp->type != type) {
        fprintf(
            stderr,
            "ERROR (line %d): the type of variable %s is not %s, but %s\n",
            lineno,id,int2type(type),int2type(tmp->type)
        );
        return 0;
    }
    tmp->value = value;
    return 1;
}

int getVariable(stack* s, char* id){
    element tmp = find(s,id);
    if (tmp == NULL) {
        fprintf(stderr,"ERROR (line %d): variable %s is not defined\n",lineno,id);
        return 0;
    }
    return tmp->value;
}

int getType(stack* s, char *id){
    element tmp = find(s,id);
    if (tmp == NULL) {
        fprintf(stderr,"ERROR (line %d): variable %s is not defined\n",lineno,id);
        return -1;
    }
    return tmp->type;
}
