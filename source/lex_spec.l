%option noyywrap

%{
// STL
#include<string.h>
#include<stdio.h>
#include<stdlib.h>

// Project files
#include "parser.h" // generated by Bison
#include "globals.h"

extern YYSTYPE yylval;

%}

WS       [ \t]
LETTER   [A-Za-z]
DIGIT    [0-9]
NZDIGIT  [1-9]
NUMBER   {NZDIGIT}{DIGIT}*|{DIGIT}
ID       (_|{LETTER})({LETTER}|{DIGIT}|_)*
TYPE     int|bool
RELOP    "<"|">"|"<="|">="|"=="|"!="
OP       "+"|"-"|"*"|"/"
BOP      and|or|not
BOOLEAN  True|False

%%

{NUMBER}  {
    yylval.number = yytext;
    return NUMBER;
}

{BOOLEAN}   {
    yylval.boolean = (strcmp(yytext,"True") == 0);
    return BOOLEAN;
}

{TYPE}    {
    yylval.type = (strcmp("int",yytext) == 0);
    return TYPE;
}

if        { return IF; }
then      { return THEN; }
else      { return ELSE; }
while     { return WHILE; }
do        { return DO; }
for       { return FOR; }
to        { return TO; }
"{"       { return OBLOCK; }
"}"       { return CBLOCK; }
"("       { return OPAR; }
")"       { return CPAR; }
print     { return PRINT;}
=         { return ASSIGN;}
:         { return TSEP; }
";"       { return ENDSTMT; }

{RELOP}   {
    if (strcmp(yytext,"<") == 0)
        return LT;
    if (strcmp(yytext,"<=") == 0)
        return LTE;    
    if (strcmp(yytext,">") == 0)
        return GT;
    if (strcmp(yytext,">=") == 0)
        return GTE;
    if (strcmp(yytext,"==") == 0)
        return EQ;
    return NEQ;
}

{BOP}     {
    if (strcmp(yytext,"and") == 0)
        return AND;
    if (strcmp(yytext,"or") == 0)
        return OR;
    return NOT;
}

{OP}      {
    if (strcmp(yytext,"+") == 0)
        return ADD;
    if (strcmp(yytext,"-") == 0)
        return SUB;
    if (strcmp(yytext,"*") == 0)
        return MUL;
    return DIV;
}

{ID}      {
    if (yyleng > 31){
        fprintf(
            stderr,
            "ERROR (line %d): Too long identifier\n",
            lineno
        );
        error = 1;
    }
    yylval.identifier = strdup(yytext);
    return ID;
}

{WS}+     {}
\n        { lineno++; }
.         {
            fprintf(
                stderr,
                "ERROR (line %d): Invalid character ('%s')\n",
                lineno,yytext
            );
            error = 1;
        }

%%