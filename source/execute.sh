#!/bin/sh
echo "Compiling file \"$1\""

./parser Cready < $1 > $2

echo "File \"$2\" created"

if [ -s $2 ]; then
  echo "Compiling and executing file \"$2\"\n"
  gcc $2 globals.c variable.c stack.c
  ./a.out

  rm a.out
fi