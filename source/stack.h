#ifndef STACK_H
#define STACK_H

#include <stdio.h>
#include "variable.h"
#include "uthash.h"
#include "globals.h"

typedef variable* element;

typedef struct stack {
    int top;
    int capacity;
    element* contents;
} stack;

void init(stack* s);
void destroy(stack *s );

void push(stack* s, element data);
void pop(stack* s);
element* peek(stack* s,int n);
element find(stack* s, char* id);
int isempty(stack* s);
int isfull(stack* s);

int addVariable(stack* s, char* id, int type);
int setVariable(stack* s, char* id, int type, int value);
int getVariable(stack* s, char* id);
int getType(stack*s, char* id);


#endif