#ifndef VARIABLE_H
#define VARIABLE_H

#include<stdio.h>
#include "uthash.h"
#include "globals.h"

typedef struct variable {
    const char* id;
    int type;
    int value;
    UT_hash_handle hh;
} variable;

int addVar(char *id, int type, variable** symTable);
int setVar(char *id, int type ,int value, variable** symTable);
int getVar(char *id, variable** symTable);
int getVarType(char *id, variable** symTable);
void delete_all(variable** symTable);
#endif