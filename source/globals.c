#include "globals.h"

int lineno = 1;
int error = 0;

char* int2type(int tvalue){
    switch (tvalue){
        case 0:
            return "'bool'";
        case 1:
            return "'int'";
        default:
            return "unknown type";
    }
}
