%{
// STL
#include <ctype.h>
#include <stdio.h>
#include <string.h>

// Project files
#include "stack.h"
#include "variable.h"
#include "globals.h"

int yylex();
void yyerror(char *s);

// Variabili per gli identificatori
stack s;
char tmp[128] = "";

// Contatori
int label = 0;
int varcount = 0;

// Variabili per l'output
FILE* out;
int testready = 0;

// Funzioni di controllo di tipo
int checkType(int type1,int type2,int expected1, int expected2,char* op) ;
int checkTypeUnary(int type,int expected,char* op);
int checkCondition(int type);
%}

%code requires {
  // Valore e tipo delle espressioni
  typedef struct attributes{
    char addr[64];
    int type;
  } attributes;

  // Etichette per i marker
  typedef struct marker{
    int l1;
    int l2;
  } marker;
}

%union{
  char* number;
  char* identifier;
  int type;
  int boolean;
  char addr[128];
  attributes info;
  marker mkr;
}

// Token di litterali, identificatori e tipi
%token <number> NUMBER
%token <boolean> BOOLEAN
%token <identifier> ID
%token <type> TYPE

// Token di keywords e caratteri dedicati agli statements
%token OBLOCK CBLOCK 
%token IF THEN ELSE WHILE DO FOR TO PRINT ASSIGN TSEP ENDSTMT

// Token per le espressioni
%token OPAR CPAR
%token ADD SUB MUL DIV
%token LT LTE GT GTE EQ NEQ
%token NOT AND OR

// Dichiarazioni di tipo per le produzioni
%type  <addr> varinit assign
%type  <info> expr cmp
%type  <mkr> m_if m_while m_for

// Precedenze degli operatori logici
%left OR
%left AND
%right NOT

// Precedenze degli operatori di confronto
%nonassoc EQ NEQ LT LTE GT GTE

// Precedenze per gli operatori aritmetici
%left ADD SUB
%left MUL DIV
%right UMINUS
%right UPLUS

// Precedenze per if-else
%nonassoc NO_ELSE 
%nonassoc ELSE

%%

// Inizio del programma
start : { fprintf(out,"init(&s);\n"); init(&s); } 
        program 
        { fprintf(out,"destroy(&s);\n"); destroy(&s); }

// Programma
program : program stmt
        | 
        ;

// Token di sincronizzazione in caso di errore
sync_token  : OBLOCK | CBLOCK | ENDSTMT ;

// Statement semplice o composto
stmt    : simplestmt ENDSTMT
        | OBLOCK openb program closeb CBLOCK
        | conditional
        | iteration
        | error sync_token { yyerrok; }
        ;

// Istruzione semplice
simplestmt  : varinit
            | assign
            | print
            |
            ;

// Apertura di blocco
openb     : { push(&s,NULL); fprintf(out,"push(&s,NULL);\n"); }

// Chiusura di blocco
closeb    : { pop(&s); fprintf(out,"pop(&s);\n"); }

// Condizione
condit      : expr {
                error = !checkCondition($1.type) || error; 
                fprintf(
                  out,
                  "if (!%s) goto L%d;\n",
                  $1.addr,($<mkr>0).l2
                );
              };

// Costrutto if/if-else
conditional : IF m_if condit THEN openb stmt closeb %prec NO_ELSE {
                fprintf(out,"L%d:",$2.l2);
                if (testready)
                  fprintf(out,";");
                fprintf(out,"\n");
              }
            | IF m_if condit THEN openb stmt closeb ELSE n_if openb stmt closeb {
                fprintf(out,"L%d:",$2.l1);
                if (testready)
                  fprintf(out,";");
                fprintf(out,"\n");
              }
            ;

// Marker per if/if-else
m_if : { $$.l1 = label++; $$.l2 = label++; };

n_if :  { 
          fprintf(
            out,
            "goto L%d;\nL%d:",
            ($<mkr>-6).l1,($<mkr>-6).l2); 
          if (testready)
            fprintf(out,";");
          fprintf(out,"\n");
        }

// Costrutti iterativi
iteration   : while 
            | for 
            ;

// Costrutto while
while       : WHILE m_while condit DO openb stmt closeb {
                fprintf(out,"goto L%d;\n",$2.l1); 
                fprintf(out,"L%d:",$2.l2);
                if (testready)
                  fprintf(out,";");
                fprintf(out,"\n");
              };

// Marker per while
m_while : { 
            $$.l1 = label++; 
            $$.l2 = label++; 
            fprintf(out,"L%d:",$$.l1);
            if (testready)
              fprintf(out,";");
            fprintf(out,"\n");
          };

// Costrutto for
for     : openb FOR varinit TO expr m_for openb stmt closeb n_for closeb
        | openb FOR assign TO expr m_for openb stmt closeb n_for closeb
        ;

// Marker per for
m_for   : {
            $$.l1 = label++; 
            $$.l2 = label++; 
            fprintf(out,"L%d:",$$.l1);
            if (testready)
              fprintf(out,";");
            fprintf(out,"\n");
            if (testready)
              fprintf(out,"int ");
            fprintf(
              out,
              "t%d = getVariable(&s,\"%s\") <= %s;\nif (!t%d) goto L%d;\n",
              varcount,($<addr>-2),($<info>0).addr,varcount,$$.l2
            ); 
            varcount++;
          }

n_for   : {
            if (testready)
              fprintf(out,"int ");
            fprintf(
              out,
              "t%d = getVariable(&s,\"%s\") + 1;\nsetVariable(&s,\"%s\",%d,t%d);\ngoto L%d;\nL%d:",
              varcount,($<addr>-6),($<addr>-6),1,varcount,($<mkr>-3).l1,($<mkr>-3).l2
            );
            if (testready)
              fprintf(out,";");
            fprintf(out,"\n");
            varcount++;
          }

// Istruzione di stampa
print : PRINT OPAR expr CPAR   { 
            fprintf(
              out,
              "printf(\"%s\",%s);\n","%d",
              $3.addr
            ); 
          }
      | PRINT OPAR CPAR        { fprintf(out,"printf(\"\\n\");\n"); }
      ;

// Istruzione di assegnazione
assign  : ID ASSIGN expr   {
            error = !setVariable(&s,$1,$3.type,0) || error; // serve per verificare la definizione della variabile e il type matching
            fprintf(
              out,
              "setVariable(&s,\"%s\",%d,%s);\n",
              $1,$3.type,$3.addr
            );
            strcpy($$,$1);
          }
        ;

// Istruzione di inizializzazione
varinit : ID TSEP TYPE              { 
            error = !addVariable(&s,$1,$3) || error; 
            fprintf(out,"addVariable(&s,\"%s\",%d);\n",$1,$3); 
            strcpy($$,$1);
          }
        | ID TSEP TYPE ASSIGN expr  {
            error = !addVariable(&s,$1,$3) || error;        // serve per verificare la presenza di definizioni multiple
            error = !setVariable(&s,$1,$5.type,0) || error; // serve per verificare il type matching
            fprintf(
              out,
              "addVariable(&s,\"%s\",%d);\nsetVariable(&s,\"%s\",%d,%s);\n",
              $1,$3,$1,$5.type,$5.addr
            );
            strcpy($$,$1);
          }
        ;

// Espressioni logiche e aritmetiche
expr  : expr ADD expr         {
          error = !checkType($1.type,$3.type,1,1,"+") || error;
          $$.type = 1; 
          tmp[0] = '\0'; 
          sprintf(tmp,"t%d",varcount++); 
          strcpy($$.addr,tmp);
          if (testready)
            fprintf(out,"int ");
          fprintf(out,"%s = %s + %s;\n",tmp,$1.addr,$3.addr);
        }
      | expr SUB expr         { 
          error = !checkType($1.type,$3.type,1,1,"-") || error;
          $$.type = 1; 
          tmp[0] = '\0'; 
          sprintf(tmp,"t%d",varcount++); 
          strcpy($$.addr,tmp); 
          if (testready)
            fprintf(out,"int ");
          fprintf(out,"%s = %s - %s;\n",tmp,$1.addr,$3.addr);
        }
      | expr MUL expr         { 
          error = !checkType($1.type,$3.type,1,1,"*") || error;
          $$.type = 1;
          tmp[0] = '\0'; 
          sprintf(tmp,"t%d",varcount++); 
          strcpy($$.addr,tmp); 
          if (testready)
            fprintf(out,"int ");
          fprintf(out,"%s = %s * %s;\n",tmp,$1.addr,$3.addr); 
        }
      | expr DIV expr         { 
          error = !checkType($1.type,$3.type,1,1,"/") || error;
          $$.type = 1;
          tmp[0] = '\0'; 
          sprintf(tmp,"t%d",varcount++); 
          strcpy($$.addr,tmp); 
          if (testready)
            fprintf(out,"int ");
          fprintf(out,"%s = %s / %s;\n",tmp,$1.addr,$3.addr); 
        }
      | OPAR expr CPAR        { 
          $$.type = $2.type;
          strcpy($$.addr,$2.addr);
        }
      | SUB expr %prec UMINUS { 
          error = !checkTypeUnary($2.type,1,"-") || error;
          $$.type = 1;
          tmp[0] = '\0'; 
          sprintf(tmp,"t%d",varcount++); 
          strcpy($$.addr,tmp); 
          if (testready)
            fprintf(out,"int ");
          fprintf(out,"%s = -%s;\n",tmp,$2.addr); 
        }
      | ADD expr %prec UPLUS  { 
          error = !checkTypeUnary($2.type,1,"+") || error;
          $$.type = 1;
          strcpy($$.addr,$2.addr); 
        }
      | expr OR expr {
          error = !checkType($1.type,$3.type,0,0,"or") || error;
          $$.type = 0;
          tmp[0] = '\0';
          sprintf(tmp,"t%d",varcount++); 
          strcpy($$.addr,tmp); 
          if (testready)
            fprintf(out,"int ");
          fprintf(out,"%s = %s || %s;\n",tmp,$1.addr,$3.addr); 
        }
      | expr AND expr {
          error = !checkType($1.type,$3.type,0,0,"and") || error;
          $$.type = 0;
          tmp[0] = '\0'; 
          sprintf(tmp,"t%d",varcount++); 
          strcpy($$.addr,tmp); 
          if (testready)
            fprintf(out,"int ");
          fprintf(out,"%s = %s && %s;\n",tmp,$1.addr,$3.addr); 
        }
      | NOT expr       {
          error = !checkTypeUnary($2.type,0,"not") || error;
          $$.type = 0;
          tmp[0] = '\0'; 
          sprintf(tmp,"t%d",varcount++); 
          strcpy($$.addr,tmp); 
          if (testready)
            fprintf(out,"int ");
          fprintf(out,"%s = !%s;\n",tmp,$2.addr);
        }
      | cmp   {
          $$.type = $1.type;
          strcpy($$.addr,$1.addr);
        }
      | BOOLEAN         { $$.type = 0; sprintf($$.addr,"%d",$1); }
      | NUMBER          { $$.type = 1; strcpy($$.addr,$1); }
      | ID              {
          $$.type = getType(&s,$1);
          sprintf($$.addr,"getVariable(&s,\"%s\")",$1);
          error = error || ($$.type == -1);
        }
      ;

// Operazioni di confronto
cmp : expr LT expr      {                
        error = !checkType($1.type,$3.type,1,1,"<") || error;
        $$.type = 0;
        tmp[0] = '\0'; 
        sprintf(tmp,"t%d",varcount++); 
        strcpy($$.addr,tmp); 
        if (testready)
          fprintf(out,"int ");
        fprintf(out,"%s = %s < %s;\n",tmp,$1.addr,$3.addr);
      }
    | expr LTE expr     { 
        error = !checkType($1.type,$3.type,1,1,"<=") || error;
        $$.type = 0;
        tmp[0] = '\0'; 
        sprintf(tmp,"t%d",varcount++); 
        strcpy($$.addr,tmp); 
        if (testready)
          fprintf(out,"int ");
        fprintf(out,"%s = %s <= %s;\n",tmp,$1.addr,$3.addr); 
      }
    | expr GT expr      { 
        error = !checkType($1.type,$3.type,1,1,">") || error;
        $$.type = 0;
        tmp[0] = '\0'; 
        sprintf(tmp,"t%d",varcount++); 
        strcpy($$.addr,tmp); 
        if (testready)
          fprintf(out,"int ");
        fprintf(out,"%s = %s > %s;\n",tmp,$1.addr,$3.addr);
      }
    | expr GTE expr     { 
        error = !checkType($1.type,$3.type,1,1,">=") || error;
        $$.type = 0;
        tmp[0] = '\0'; 
        sprintf(tmp,"t%d",varcount++); 
        strcpy($$.addr,tmp); 
        if (testready)
          fprintf(out,"int ");
        fprintf(out,"%s = %s >= %s;\n",tmp,$1.addr,$3.addr);
      }
    | expr EQ expr      { 
        error = !checkType($1.type,$3.type,1,1,"==") || error;
        $$.type = 0;
        tmp[0] = '\0'; 
        sprintf(tmp,"t%d",varcount++); 
        strcpy($$.addr,tmp); 
        if (testready)
          fprintf(out,"int ");
        fprintf(out,"%s = %s == %s;\n",tmp,$1.addr,$3.addr);
      }
    | expr NEQ expr     { 
        error = !checkType($1.type,$3.type,1,1,"!=") || error;
        $$.type = 0;
        tmp[0] = '\0'; 
        sprintf(tmp,"t%d",varcount++); 
        strcpy($$.addr,tmp); 
        if (testready)
          fprintf(out,"int ");
        fprintf(out,"%s = %s != %s;\n",tmp,$1.addr,$3.addr); 
      }
    ;
%%

int main(int argc, char** argv){

  testready = (argc > 1) && !strcmp(argv[1],"Cready");

  out = tmpfile();

  if (testready)
    fprintf(out,"#include<stdio.h>\n#include\"stack.h\"\n\nint main() {\nstack s;\n");

  if (yyparse() != 0) {
    error = 1;
    fprintf(stderr,"EOF reached with error: check for unclosed blocks or incomplete statements\n");
  }

  if (testready)
    fprintf(out,"}\n");

  if (!error) { // se non ci sono stati errori sposto il codice generato in stdout
    rewind(out);
    char buffer [256];
    while (!feof(out)) {
      if (fgets(buffer,256,out) == NULL) break;
      fputs(buffer,stdout);
    }
  }

  fclose(out);
  return 0;
}

void yyerror(char *s) {
  error = 1;
  fprintf(stderr,"ERROR (line %d): %s\n",lineno,s);
}

int checkType(int type1,int type2,int expected1, int expected2,char* op) {
  if ((type1 == -1) || type2 == -1)
    return 0;
  
  if ((type1 != expected1) || (type2 != expected2)){
    fprintf(
      stderr,
      "ERROR (line %d): the types expected by operator '%s' are %s and %s, but %s and %s given\n",
      lineno,op,int2type(expected1),int2type(expected2),int2type(type1),int2type(type2)
    );
    return 0;
  }
  return 1;
}

int checkTypeUnary(int type,int expected,char* op){
  if (type == -1)
    return 0;

  if (type != expected) {
    fprintf(
      stderr,
      "ERROR (line %d): the type expected by operator '%s' is %s, but %s given\n",
      lineno,op,int2type(expected),int2type(type)
    );
    return 0;
  }
  return 1;
}

int checkCondition(int type) {
  if (type == -1)
    return 0;
  
  if (type > 0) {
    fprintf(
      stderr,
      "ERROR (line %d): the types expected for the condition is 'bool', but %s given\n",
      lineno,int2type(type)
    );
    return 0;
  }
  return 1;
}